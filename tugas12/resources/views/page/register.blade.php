<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="firstName"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lastName"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio">Male <br>
        <input type="radio">Female <br>
        <input type="radio">Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="ind">Indonesia</option>
            <option value="sgp">Singapura</option>
            <option value="jpg">Jepang</option>
            <option value="knd">Kanada</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa">English <br>
        <input type="checkbox" name="bahasa">Other <br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up" >
        
        
    </form>
</body>
</html>