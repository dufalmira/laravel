<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view('page.register');
    }

    public function masuk(Request $request){
        $fName = $request['firstName'];
        $lName = $request['lastName'];

        return view('page.wel', ['namaDepan' => $fName, 'namaBelakang' => $lName]);
    }
}
